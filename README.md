
---

TeamEverywhere Nodejs Guide
*Updated at 2019-12-01*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*

- [서버란_무엇인가?](#서버란_무엇인?)
- [네트워크세계](#네트워크세계)
- [NodejS 서버](#NodejS 서버)
- [프로토콜](#프로토콜)
- [http프로토콜](#http프로토콜)
- [WWW](#WWW)
- [IP_Address](#IP_Address)
- [port](#port)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## 서버란_무엇인가?
    - 서버 또한 하나의 컴퓨터입니다.
    - 지금 우리가 가지고 있는 노트북 또한 하나의 서버가 될 수 있습니다.
    - 서버란 원격으로 접속할 수 있도록 특정 Port를 오픈 해 놓은 컴퓨터라고 할 수 있습니다.
    - 이를 개발자 언어로 표현하면 다음과 같습니다.
    - 서버(영어: server)는 클라이언트에게 네트워크를 통해 정보나 서비스를 제공하는 컴퓨터 시스템을 의미한다


## 네트워크세계
    - 우리가 미국에 서버가 있는 홈페이지에 접속할 수 있는 이유는 뭘까요?
    - 셀제로는 아래 그림과 같이 전세계적으로 케이블이 연결되어 있습니다.
    - 아쉽게도 대륙을 횡단할 수 있는 와이파이 기술은 아직 없으니까요
    - 이제 그럼 왜 미국에 있는 사이트를 접속할때, 우리가 느린 지 유추해볼 수 있습니다!
<img src="cable_image.jpeg" width="640" height="360" />


## NodejS 서버
    - 아래 예제는 NodeJS 공식 웹사이트에 가면 바로 볼 수 있는 서버 코드 입니다.
```javascript
    const http = require('http');
    //우리가 만드는 서버는 http 프로토콜을 사용하는 서버 입니다.
    //NodeJS로 HTTP 서버 개발을 가능하게 해주는 모듈

    const hostname = '127.0.0.1';
    //호스트란 우리가 접속 하는 ip 주소를 의미 합니다.
    
    const port = 3000;
    
    const server = http.createServer((req, res) => {
        //웹 서버 객체를 생성하며, 클라이언트에서 호출 될때마다 실행됩니다.
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Hello World\n');
    });

    server.listen(port, hostname, () => {
        //이미 만든 객체를 전달 받은 포트로 활성화 합니다.
        console.log(`Server running at http://${hostname}:${port}/`);
    });
```


## 프로토콜
    - 원거리 통신 장비에서 메세지를 주고 받을 때 사용하는 규칙과 양식의 체계


## 모듈
    - 독립된 기능을 하는 하나의 소프트웨어


## http프로토콜
    - Hyper Text Transfer Protocol
    - WWW 상에서 정보를 주고받을 수 있는 프로토콜
    - TCP와 UDP를 사용하며 80번 포트를 사용한다.
    - 유저가 어떠한 홈페이지나 앱을 통해 서버에 있는 자료를 요청할때에 주로 사용하는 통식 방식
    - header : 전송하는 데이터의 타입 등을 규정합니다.
    - body : 실제로 데이터를 전송하는 부분
    - request : 클라이언트(프론트엔드)로부터 전달받은 요청
    - response : 클라이언트로 전달하는 응답


## WWW
    - 인터넷을 통해 사람들이 정보를 공유할 수 있는 전 세계적인 정보 공간
    - 아래와 같은 세가지의 기능으로 WWW의 특성을 정의 할 수 있다.
        - URL이라는 위치 지정 방식
        - 서버 등 웹 자원에 접근할 수 있게 하는 프로토콜 (HTTP)
        - 유저가 웹 자원을 검색하고 확인할 수 있게 도와주는 언어들 (HTML)


## IP_Address    
    - IP : Internet Protocol Address란 네트워크 상에서 디바이스 (네트워크 장치)들이 서로를 인식하기 위한 주소 체계입니다.
    - 127.0.0.1은 로컬 주소를 이용하며, 로컬 컴퓨터 (즉 지금 내 컴퓨터!)로 통하는 주소를 의미 합니다.
    - 로컬 아이피는 일명 루트백 아이피라고 합니다.


## port 
    - 네트워크를 식별하는 단위 (0~65535)
    - 일반적으로 약속된 포트는 아래와 같습니다.    
    - 22 : SSH    
        - 네트워크 상의 다른 컴퓨터에 로그인하거나 원격 시스템에서 명령을 실행하고 다른 시스템으로 파일을 복사할 수 있도록 해 주는 응용 프로그램 또는 프로토콜
        - 강력한 인증방법을 사용할 수 있고, 모든 통신은 암호와 되어 전송된다.
        - 나중에 AWS 등의 서버를 조작할때, GUI라고 하는 관리자 페이지 보다 SSH가 더 익숙한 자신을 발견하게 되실 겁니다.
    - 80 : 월드 와이드 웹 HTTP
    - 443 : TLS/SSL 방식의 HTTP
        - SSL (Secure Sockets Laye)
        - TLS (Transport Layer Security) : SSL v3.0 을 표준화 한 것이 TLS 1.0
        - 데이터의 전송 과정에서 서버에 대한 인증 및 암호화를 통한 보안 강화 통신 방식

