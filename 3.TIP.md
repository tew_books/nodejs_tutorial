
---

TeamEverywhere Nodejs Guide
*Updated at 2019-12-01*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*

- [NODEMON](#NODEMON)
- [POSTMAN](#POSTMAN)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
## NODEMON
    - 코드 수정 시 자동으로 새로고침을 해주는 모듈!!
    - npm init
    - npm install -g nodemon (설치가 안될시, sudo npm install -g nodemon)
    - npm install --save-dev nodemon    
    

## POSTMAN
    - API테스트를 쉽게 해줄 수 있는 툴

