
---

TeamEverywhere Nodejs Guide
*Updated at 2019-12-01*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*


- [Firebase](#Firebase)



<!-- END doctoc generated TOC please keep comment here to allow auto update -->
## Firebase
  - Firebase란 구글에서 제공하는 개발 지원 서비스 입니다.
  - Realtime database를 제공하기도 하고, 일반적인 앱에서 사용하는 푸쉬 기능을 지원하기도 합니다.
  - https://firebase.google.com/?hl=ko


## 푸쉬란?
  - 어떠한 전송 요청이 중앙 서버에서 시작되어 각 단말로 하달되는 정보 전달 방식입니다.
    - 반대로 클라이언트로부터 서버로 전송을 한다면 풀 기법이라고 합니다.
  - 일반적으로는 특정 공지사항을 앱에 보낸다든지, 카톡이 오면 카톡 알림창이 뜬다든지 하는 기능을 구현할 때에 사용합니다.
  - 예전에는 GCM을 사용했으나, 요즘은 GCM의 최신 버전이라고도 할 수 있는 Firebase Cloud Messaging을 주로 사용하고 있습니다.
    - 공짜 거든요!


## Cloud Messaging
  - 자 그럼 이제 Firebase의 Cloud Messaging을 이용해서 푸쉬 서버를 만들어 보겠습니다.
  - 이렇게 한번 서버를 만들어 놓으면, 그 후 부터는 웹이나 앱 클라이언트에 푸쉬를 보낼 수 있습니다.
  - Firebase Docs : https://firebase.google.com/docs/cloud-messaging/
  - Firebase JS Sample Code 
    - https://github.com/firebase/quickstart-js
  - Firebase Console
    - 설정 - 서비스 계정 - Firebase Admin SDK - 새 비공개 키 생성
    - 생성 되는 정보를 아래와 같이 저장 합니다.    
  
```javascript
  // components/firebase/index.js
  'use strict'
  const admin = require('firebase-admin')
  const serviceAccount = require('./serviceAccountKey')

  let account, databaseURL


  account = serviceAccount.development
  databaseURL = "https://nodetutorial-b3c87.firebaseio.com"

  admin.initializeApp({
    credential: admin.credential.cert(account),
    databaseURL: databaseURL
  })
  // console.log("admin : ", admin)
  module.exports = admin
```

  - 이제 다음과 같은 코드로 fcm 메세지를 보낼 수 있습니다.
  - components 폴더에 fcm.js를 만들어 봅시다.
  
```javascript
  const fcm = require('./firebase/index')
  module.exports.sendToTopic = async (options) => {
    return new Promise(function (resolve, reject) {
      try {
        fcm.messaging().send(options)
        .then(function(response){
          console.log('response : ',response)
          resolve(true)
        })
        .catch(function(err){
          reject(err)
        })
      }
      catch (err) {
        throw new Error(err)
        // console.log(err)
      }
    })
    
  }
```

- Firebase Push Message는 Https 기반 웹프론트, 안드로이드, IOS 앱 등에서 수신할 수 있습니다.


